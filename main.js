"use strict";

const ipButton = document.getElementById("ip-btn");
const ipContent = document.getElementById("ip-content");

ipButton.addEventListener("click", async () => {
  try {
    const response = await fetch("http://api.ipify.org/?format=json");
    const data = await response.json();
    const ipAddress = data.ip;
    const geoResponse = await fetch(
      `http://ip-api.com/json/${ipAddress}?fields=1572889`
    );
    const geoData = await geoResponse.json();
    const continent = geoData.continent;
    const country = geoData.country;
    const region = geoData.regionName;
    const city = geoData.city;
    const district = geoData.district;
    ipContent.innerHTML = `<p>Континент: ${continent}</p>
                            <p>Країна: ${country}</p>
                            <p>Регіон: ${region}</p>
                            <p>Місто: ${city}</p>
                            <p>Район: ${district}</p>`;
  } catch (error) {
    ipContent.innerHTML = `<p>Помилка: ${error.message}</p>`;
  }
});
